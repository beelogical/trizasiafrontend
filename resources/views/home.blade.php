@extends('layouts.app')

@section('content')
<?php 
    $serverIp = config('constants.server');
?>
<div class="dashboard login-Page">
    <div class="container">
        <div class="main">
            <div class="col-md-3">
                <div class="sidebar">
                    <ul>
                        <li class="active"><a href="{{ url('home') }}"><i class="fa fa-tachometer" ></i> Dashboard</a></li>
                        <li><a href="{{ url('uservideo') }}"><i class="fa fa-video-camera" ></i> Videos</a></li>
                        <li><a href="{{ url('usermembership') }}"><i class="fa fa-users" ></i> Membership plans</a></li>
                        <li><i class="fa fa-money" ></i> Payment Pending</li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/"><i class="fa fa-arrow-left" ></i> Go to Website</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="dash_Data">
                    <div class="dashboardTab">
                        <div class="tabhead">
                            <h1>DASHBOARD</h1>
                        </div>
                        <div class="col-md-4">
                            Total Videos : {{ $totalVideos }}
                        </div>

                        <div class="col-md-5">
                            Membership plan : 
                            <ul>
                            @php  $sn = 1;@endphp
                            @foreach($memberShip as $item) 
                                <li>Title : {{ $item->title }}</li>
                                <li>Description : {{ $item->description }}</li>
                                <li>Price : {{ $item->cost }}</li>
                                <li>Validity : {{ $item->validity }}</li>
                            @php  $sn++;@endphp   
                            @endforeach
                            </ul>
                        </div>
                    </div>
                </div>            
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection

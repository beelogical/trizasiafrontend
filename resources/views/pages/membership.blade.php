@extends('layouts.app')

@section('content')
<div class="videoPage login-Page commoncss">
    <div class="container">
    <h2> Membership <span>Plans</span></h2>
        @php  $sn = 1;@endphp
        @foreach($data as $item) 
        <div class="col-md-4">
            <div class="videoBox">
                <div class="package-header">
                    <div class="border"></div> 
                    <h1><span>{{ $item->title }}</span></h1>
                </div>
                <div class="col-md-4 no-pad text-center">
                    <div class="borderRight">
                        <p>COST<span>{{ $item->cost }}</span></p>
                    </div>
                </div>
                <div class="col-md-4 no-pad text-center">
                    <p>VALIDITY<span>{{ $item->validity }}</span> </p>
                </div>
                <div class="clearfix"></div>

                <ul class="membershpipFeature">
                    <li>{{ $item->description }}</li>
                </ul>
                <div class="text-center">
                    <button class="btn-primary">Buy Now</button>
                </div>
            </div>
        </div>
        @php  $sn++;@endphp   
        @endforeach
    </div>
</div>
@endsection

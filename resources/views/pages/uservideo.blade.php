@extends('layouts.app')

@section('content')
<?php 
    $serverIp = config('constants.server');
?>
<div class="dashboard login-Page">
    <div class="container">
        <div class="main">
            <div class="col-md-3">
                <div class="sidebar">
                    <ul>
						<li><a href="{{ url('home') }}"><i class="fa fa-tachometer" ></i> Dashboard</a></li>
                        <li class="active"><a href="{{ url('uservideo') }}"><i class="fa fa-video-camera" ></i> Videos</a></li>
                        <li><a href="{{ url('usermembership') }}"><i class="fa fa-users" ></i> Membership plans</a></li>
                        <li><i class="fa fa-money" ></i> Payment Pending</li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/"><i class="fa fa-arrow-left" ></i> Go to Website</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="dash_Data">
                    <div class="dashboardTab">
                        <div class="tabhead">
                            <h1>VIDEOS</h1>
                        </div>
                        <div class="table-responsive pad-30">
                            <table id="videoTable" class="display table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Video Title</th>
                                        <th>Status</th>
                                        <th>Video Link</th>
                                        <th>Price</th>
                                        <th>Description</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php  $sn = 1;@endphp
                                @foreach($data as $item)  
                                    <tr>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->status }}</td>
                                        <td>{{ $item->video }}</td>
                                        <td>{{ $item->price }}</td>
                                        <td>{{ $item->body }}</td>
                                    </tr>
                                    @php  $sn++;@endphp   
                                 @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@endsection



@extends('layouts.app')

@section('content')
<div class="videoPage login-Page">
    <div class="container">
    @php  $sn = 1;@endphp
        @foreach($data as $item) 
        <div class="col-md-4">
            <div class="videoBox">
                <div class="imgBox">
                    <img src="{{ $item->image }}" />
                </div>
                <h1>{{ $item->title }}</h1>
                <p>COST : <span>{{ $item->price }}</span></p>
                <a href="{{ url('watchvideo') }}"><button class="btn-primary">Watch Now</button></a>
            </div>
        </div>
        <!-- <div class="col-md-4">
            <div class="videoBox">
                <div class="imgBox">
                    <img src="images/video3.jpeg" />
                </div>
                <h1>The Complete Communication Skills Master Class for Life</h1>
                <p>COST : <span>$10.23</span></p>
                <a href="{{ url('watchvideo') }}"><button class="btn-primary">Watch Now</button></a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="videoBox">
                <div class="imgBox">
                    <img src="images/video2.jpg" />
                </div>
                <h1>The Complete Communication Skills Master Class for Life</h1>
                <p>COST : <span>$10.23</span></p>
                <a href="{{ url('watchvideo') }}"><button class="btn-primary">Watch Now</button></a>
            </div>
        </div> -->
        @php  $sn++;@endphp   
        @endforeach
    </div>
</div>
@endsection

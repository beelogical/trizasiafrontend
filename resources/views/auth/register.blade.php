@extends('layouts.app')

@section('content')
<div class="login-Page">
    <div class="col-md-offset-2 col-md-8">
        <div class="loginContainer">
            <h1 class=" text-center">REGISTER</h1>
            <form method="POST" action="{{ route('register') }}">
            @csrf
                <div class="form-group">
                    <div class="col-md-6">
                        
                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter Name">

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                     <div class="col-md-6">

                        <input id="dob" type="text" class="form-control @error('dob') is-invalid @enderror" name="dob" value="{{ old('dob') }}" required placeholder="Date of Birth as yyyy-mm-dd">

                        @error('dob')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6">

                        <input id="mobile" type="text" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ old('mobile') }}" required autocomplete="mobile" autofocus placeholder="Mobile No." pattern="[1-9]{1}[0-9]{9}">

                        @error('mobile')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                    <div class="col-md-6">

                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter Email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6">

                        <input id="officialemail" type="email" class="form-control @error('officialemail') is-invalid @enderror" name="officialemail" value="{{ old('officialemail') }}" required autocomplete="officialemail" placeholder="Enter Official Email">

                        @error('officialemail')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                
                    <div class="col-md-6">

                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter Password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6">
                        <textarea placeholder="Profile" name="profile" class="form-control"></textarea> 
                    </div>
                    <div class="col-md-6">
                        <textarea placeholder="Address" name="address" class="form-control"></textarea> 
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6">
                        <input type="text" class="form-control" required autocomplete="name" placeholder="Enter Company Name" name="company">
                    </div>
                    <div class="col-md-6">
                        <input type="text" class="form-control" required autocomplete="designation" placeholder="Enter Designation" name="designation">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="form-group">
                    <div class="col-md-6">
                        <input id="Country" type="text" class="form-control" name="place" required placeholder="Country">
                    </div>
                    <div class="col-md-6">
                        <select class="form-control" name="certificate" required="required">
                            <option value="">MATRIZ certified</option>
                            <option value="LEVEL 1">LEVEL 1</option>
                            <option value="LEVEL 2">LEVEL 2</option>
                            <option value="LEVEL 3">LEVEL 3</option>
                            <option value="LEVEL 4">LEVEL 4</option>
                            <option value="LEVEL 5">LEVEL 5</option>
                            <option value="NONE">NONE</option>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="text-center">
                    <button type="submit" class="btn-primary"> REGISTER </button>
                     <p>Already have an account? <a class="linkBtn" href="{{ url('login') }}">login here</a></p>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
     <div class="clearfix"></div>
</div>
 <div class="clearfix"></div>
@endsection

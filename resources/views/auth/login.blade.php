@extends('layouts.app')

@section('content')
<div class="login-Page">
    <div class="col-md-offset-4 col-md-4 ">
        <div class="loginContainer">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <h1 class="text-center">LOGIN</h1>
                    <div class="col-md-12">
                        <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                            </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn-primary m-r-20">LOGIN</button>
                        <button class="btn-primary">FORGOT PASSWORD</button>
                        <p>Don't have an account? <a href="{{ url('register') }}">Register here</a></p>
                    </div>
                </form>
            </div> 
        </div>
    <div class="clearfix"></div>
</div>
@endsection

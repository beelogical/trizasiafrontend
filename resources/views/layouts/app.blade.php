<!DOCTYPE html>
<html>
<head>
    <title>Trizasia</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1" user-scalable="no">
    
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.js') }}"></script>
</head>
<body>
    <?php 
        $serverIp = config('constants.server');
    ?>
<section class="header-section">
    <div class="topBar">
        <div class="container">
            <div class="lefttopHeader">
                <p><span><i class="fa fa-clock-o" aria-hidden="true"></i></span> Monday - Friday <span class="time">8AM - 9PM</span></p>
            </div>
            <div class="righttopHeader">
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link pad-right-20" href="{{ route('login') }}">{{ __('LOG IN') }}</a>  | 
                        </li> 
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('REGISTER') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown " class="nav-link dropdown-toggle pad-right-20" href="{{ url('home') }}">
                                {{ Auth::user()->name }}
                            </a>  | 
                        </li>
                        <li class="" aria-labelledby="">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('LOGOUT') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                    @endguest
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="mainHeader">
        <header class="header">
            <div class="container">
                <nav class="navbar navbar-inverse">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#">
                            <img src="{{ asset('images/Logo.png') }}" >
                        </a>
                    </div>
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="http://<?php echo $serverIp; ?>/trizasso/">MAIN</a></li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1569">ABOUT US</a></li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1595">NEWS</a></li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1623">ARTICLES FROM MEMBERS</a></li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1567">CERTIFICATION</a></li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1597">MEMBERSHIP</a></li>
                        <li><a href="#">VIDEOS</a></li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=2312">TERMS</a></li>
                        <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1601">CONTACT US</a></li>
                    </ul>
                </nav>
            </div>
        </header>
    </div>
</section>

        <main class="py-4">
            @yield('content')
        </main>
<!-- START:: footer-section -->
<footer class="footer-section">
    <div class="topFooter">
        <div class="container">
            <div class="col-md-3">
                <h1>TAA</h1>
                <h2>Contact Details</h2>
                <!-- <p>Organically grow the holistic world view of disruptive innovation via empowerment.</p> -->
                <a href="#"><i class="fa fa-phone" aria-hidden="true"></i> +91 900 403 00 93 <br>  &nbsp;&nbsp;&nbsp; +91 79729 70976</a>
                <a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> info@trizassociation.org</a>
                <a href="#"><i class="fa fa-globe" aria-hidden="true"></i> trizassociation.org</a>
            </div>
            <div class="col-md-6">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                <h1>OUR LOCATIONS</h1>
                <h2>Where to find us?</h2>
                    <img src="images/triz-map.png" alt="footer Map" height="150" width="300">
                </div>
                <div class="col-md-2"></div>
                <!-- <div class="col-md-5 loactinMap">
                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> London: 020 7946 0020</a>
                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> Ontario: 613 285 5534</a>
                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> Tokyo: 0428 298 114</a>
                </div> -->
                <div class="clearfix"></div>
            </div>
            <div class="col-md-3">
                <h1>GET IN TOUCH</h1>
                <h2>TRIZasso Social links</h2>
                <div class="soacial-icons">
                    <p><span><i class="fa fa-facebook" aria-hidden="true"></i></span></p>
                    <p><span><i class="fa fa-twitter" aria-hidden="true"></i></span></p>
                    <!-- <p><span><i class="fa fa-pinterest-p" aria-hidden="true"></i></span></p> -->
                    <p><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>  <!-- End container -->
    </div>
    <div class="bottomFooter">
        <div class="container">
            <div class="col-md-4">
            <p> Copyright by <b>Triz Association Of Asia.</b> All rights reserved.</p>
            </div>
            <div class="col-md-8 no-pad">
                <div style="float: right;">
                    <li class="active"><a href="http://<?php echo $serverIp; ?>/trizasso/">MAIN</a></li>
                    <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1569">ABOUT US</a></li>
                    <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1595">NEWS</a></li>
                    <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1623">ARTICLES FROM MEMBERS</a></li>
                    <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1567">CERTIFICATION</a></li>
                    <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1597">MEMBERSHIP</a></li>
                    <li><a href="#">VIDEOS</a></li>
                    <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=2312">TERMS</a></li>
                    <li><a href="http://<?php echo $serverIp; ?>/trizasso/?page_id=1601">CONTACT US</a></li>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</footer>
<script type="text/javascript">
    $('#videoTable').DataTable();
    $("#dob").datepicker({
        dateFormat:'yy-mm-dd'
    });
</script>
<!-- END:: footer-section -->
<script src="{{ asset('js/jquery.elimore.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>


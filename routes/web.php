<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('videos', 'PagesController@videos');
Route::get('membership', 'PagesController@membership');
Route::get('/', function () {
    return redirect('http://'.config('constants.server').'/trizasso/');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/home', 'HomeController');
    Route::get('usermembership', 'PagesController@usermembership');
    Route::get('watchvideo', 'PagesController@watchvideo');
    Route::get('uservideo', 'PagesController@uservideo');
  });
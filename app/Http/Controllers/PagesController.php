<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\MembershipPlan;
use App\Post;
use App\CustomerPost;
use App\CustomerMembershipPlan;
use Storage;
class PagesController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function membership()
    { 
        $data = MembershipPlan::get()->all();
        
        return view('pages.membership', compact('data'));
    }

    public function videos()
    {
        $data = Post::get()->all();
        $dataArr = [];
        foreach($data as $key => $value){
            $dataArr[$key] = $value;
            $s3ImagePath = "Sample/" . $value->image;
            $dataArr[$key]->image = Storage::disk('s3')->url($s3ImagePath);
        }
        
        return view('pages.video', compact('data'));
    }

    public function watchvideo()
    {
        return view('pages.watchvideo');
    }

    public function uservideo()
    {   
        $data = Post::where('customer_post.customer_id', Auth::user()->id)
                ->join('customer_post', 'customer_post.post_id', '=', 'posts.id')
                ->get()->all();
        
        return view('pages.uservideo', compact('data'));
    }

    public function usermembership()
    {
        $data = MembershipPlan::where('customer_membership_plan.customer_id', Auth::user()->id)
                ->join('customer_membership_plan', 'customer_membership_plan.membership_plan_id', '=', 'membership_plans.id')
                ->get()->all();
        
        return view('pages.usermembership', compact('data'));
    }
}

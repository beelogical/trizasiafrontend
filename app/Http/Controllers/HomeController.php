<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\MembershipPlan;
use App\Post;
use App\CustomerPost;
use App\CustomerMembershipPlan;
use Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $videos = Post::where('customer_post.customer_id', Auth::user()->id)
                ->join('customer_post', 'customer_post.post_id', '=', 'posts.id')
                ->get()->all();
        $totalVideos = count($videos);
        
        $memberShip = MembershipPlan::where('customer_membership_plan.customer_id', Auth::user()->id)->where('customer_membership_plan.is_active', 1)
                ->join('customer_membership_plan', 'customer_membership_plan.membership_plan_id', '=', 'membership_plans.id')
                ->get()->all();
        
        return view('home', compact('totalVideos', 'memberShip'));
    }
}
